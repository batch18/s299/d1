let express = require("express");
const PORT = 4000;
let app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true})); 



	

	app.get("/", (req, res)=>res.send("hello world"));
	/*or this

	app.get("/", (req, res)=>{
	res.send("hello world");
	});

	*/

	app.get("/hello", (req, res)=>res.send("hello from the /hello endpoint"));

	app.post("/greeting", (req, res)=>{
		console.log(req.body);
		name = JSON.stringify(req.body.name)
		res.send(`hello, ${req.body.name}`);


	});
	
app.listen(PORT, ()=> {
	console.log(`Server running at port ${PORT}`)
});